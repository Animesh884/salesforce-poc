public class CaseService {
    public static void closeCases(set<ID> caseIds,String closeReason) {
        // Validate parameters
        if(caseIds==null || caseIds.size()==0)
            throw new CaseServiceException('Cases not specified.');
        if(closeReason==null)
            throw new CaseServiceException('Invalid Case Reason');
                List<Case> caseToUpdate = new List<Case>();
        
        for (Id caseId : caseIds)
        {
            Case c = new Case(Id = caseId);
            c.Reason = closeReason;
            c.Status = 'Closed'; 
            
            caseToUpdate.add(c);
        }
        
        // Update the database
        SavePoint sp = Database.setSavePoint();
        
        try {
            update caseToUpdate;
        } catch (Exception e) {
            // Rollback
            Database.rollback(sp);
            // Throw exception on to caller
            throw e;
        } 
        
     }
    
 	public class CaseServiceException extends Exception {} 
}