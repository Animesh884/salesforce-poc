public class RandomContactFactory {
    public static List<Contact> generateRandomContacts(Integer numberOfContacts, String lastName){
        List<Contact> contactsList = new List<Contact>();
        
        for(Integer itr=0; itr<numberOfContacts; itr++) {
            contactsList.add(new Contact ( Firstname = 'Test ' + itr,
                                          LastName = lastName
            ));
        }
        return contactsList;
    }
}