public class AccountHandler {
    public static Account insertNewAccount(String incomingAccount){
        try {
        Account accountName = new Account(Name = incomingAccount);
        insert accountName;
        return accountName;
        }
        catch (DmlException e) {
            System.debug('DML Exception : ' + e.getMessage());
            return null;
        }
    }
}