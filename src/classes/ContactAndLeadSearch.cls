public class ContactAndLeadSearch {
    public static List<List<SObject>> searchContactsAndLeads(String name){
        List<List<SObject>> searchResults = [Find :name IN NAME FIELDS RETURNING Contact(Name) , Lead(Name)];
            return searchResults;
    }
}