public class ContactSearch {
    public static List<Contact> searchForContacts(String searchLastName , String searchPostCode){
        List<Contact> contactList = [Select Id,Name from Contact where lastName = :searchLastName AND MailingPostalCode = :searchPostCode];
        return contactList;
    }
}